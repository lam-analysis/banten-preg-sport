---
format: pdf
---

```{r init, echo = FALSE, message = FALSE, warning = FALSE}

pkgs <- c("magrittr", "gtsummary", "kableExtra")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)

knitr::opts_chunk$set(
  eval = TRUE, echo = FALSE, message = FALSE, warning = FALSE, error = FALSE,
  fig.width = 10, fig.height = 8, output.width = "\\textwidth"
)

tbl.eng <- readr::read_csv("../data/processed/data-eng.csv")

```

# Subjects' characteristics

```{r}
#| tbl-cap: Summary of current symptoms and previous medical histories

tbl.eng %>%
  select(miscarriage:weightNotInc, histPremature:histTwin) %>%
  tbl_summary() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Summary of the workout habits

tbl.eng %>%
  select(weightLift:usualADL) %>%
  tbl_summary() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Fitness characteristics, grouped by `fatigue`

These tables summarize the subjects fitness, grouped based on the complaint of having (`1`) and not having (`0`) fatigue during this pregnancy.

```{r}
#| tbl-cap: Fitness parameters before workout

tbl.eng %>%
  select(fatigue, SpO2_pre:map_pre) %>%
  tbl_summary(
    by = "fatigue",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters after workout

tbl.eng %>%
  select(fatigue, SpO2_post:map_post) %>%
  tbl_summary(
    by = "fatigue",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters of the difference between pre- and post-workout

tbl.eng %>%
  select(fatigue, SpO2_diff:map_diff) %>%
  tbl_summary(
    by = "fatigue",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Fitness characteristics, grouped by `routineWalk`

These tables summarize the subjects fitness, grouped based on the habit of doing (`1`) and not doing (`0`) a routine walk during this pregnancy.

```{r}
#| tbl-cap: Fitness parameters before workout

tbl.eng %>%
  select(routineWalk, SpO2_pre:map_pre) %>%
  tbl_summary(
    by = "routineWalk",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters after workout

tbl.eng %>%
  select(routineWalk, SpO2_post:map_post) %>%
  tbl_summary(
    by = "routineWalk",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters of the difference between pre- and post-workout

tbl.eng %>%
  select(routineWalk, SpO2_diff:map_diff) %>%
  tbl_summary(
    by = "routineWalk",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Fitness characteristics, grouped by `longStand`

These tables summarize the subjects fitness, grouped based on the habit of doing (`1`) and not doing (`0`) a long duration of standing during this pregnancy.

```{r}
#| tbl-cap: Fitness parameters before workout

tbl.eng %>%
  select(longStand, SpO2_pre:map_pre) %>%
  tbl_summary(
    by = "longStand",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters after workout

tbl.eng %>%
  select(longStand, SpO2_post:map_post) %>%
  tbl_summary(
    by = "longStand",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters of the difference between pre- and post-workout

tbl.eng %>%
  select(longStand, SpO2_diff:map_diff) %>%
  tbl_summary(
    by = "longStand",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Fitness characteristics, grouped by `frequentSit`

These tables summarize the subjects fitness, grouped based on the habit of frequently (`1`) and not frequently (`0`) sitting during this pregnancy.

```{r}
#| tbl-cap: Fitness parameters before workout

tbl.eng %>%
  select(frequentSit, SpO2_pre:map_pre) %>%
  tbl_summary(
    by = "frequentSit",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters after workout

tbl.eng %>%
  select(frequentSit, SpO2_post:map_post) %>%
  tbl_summary(
    by = "frequentSit",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

```{r}
#| tbl-cap: Fitness parameters of the difference between pre- and post-workout

tbl.eng %>%
  select(frequentSit, SpO2_diff:map_diff) %>%
  tbl_summary(
    by = "frequentSit",
    type = list(where(is.numeric) ~ "continuous"),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]"
    )
  ) %>%
  add_difference() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Age to Borg scale 

This linear model indicates how age influences the difference of pre- and post-workout Borg scale as an indicator of perceived exertion. We hypothesized that subjects with good health and fitness will have minimal changes on the Borg scale. The blood pressure (`systole_pre` and `diastole_pre`) and daily physical activity (`usualADL`) are controlled as potential confounders.

```{r}
#| tbl-cap: Age to Borg scale difference

lm(borg_diff ~ age + usualADL + systole_pre + diastole_pre, data = tbl.eng) %>%
  tbl_regression() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Age to oxygen saturation

This linear model indicates how age influences the difference of pre- and post-workout oxygen saturation as an indicator of perceived exertion. We hypothesized that subjects with good health and fitness will have minimal changes on the oxygen saturation. The blood pressure (`systole_pre` and `diastole_pre`) and daily physical activity (`usualADL`) are controlled as potential confounders.

```{r}
#| tbl-cap: Age to oxygen saturation difference

lm(SpO2_diff ~ age + usualADL + systole_pre + diastole_pre, data = tbl.eng) %>%
  tbl_regression() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Age to heart rate 

This linear model indicates how age influences the difference of pre- and post-workout heart rate as an indicator of perceived exertion. We hypothesized that subjects with good health and fitness will have minimal changes on the heart rate. The blood pressure (`systole_pre` and `diastole_pre`) and daily physical activity (`usualADL`) are controlled as potential confounders.

```{r}
#| tbl-cap: Age to heart rate difference

lm(hr_diff ~ age + usualADL + systole_pre + diastole_pre, data = tbl.eng) %>%
  tbl_regression() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

# Age to respiratory rate 

This linear model indicates how age influences the difference of pre- and post-workout respiratory rate as an indicator of perceived exertion. We hypothesized that subjects with good health and fitness will have minimal changes on the respiratory rate. The blood pressure (`systole_pre` and `diastole_pre`) and daily physical activity (`usualADL`) are controlled as potential confounders.

```{r}
#| tbl-cap: Age to respiratory rate difference

lm(rr_diff ~ age + usualADL + systole_pre + diastole_pre, data = tbl.eng) %>%
  tbl_regression() %>%
  as_kable_extra(longtable = TRUE, booktabs = TRUE) %>%
  kable_styling(latex_options = "repeat_header")

```

