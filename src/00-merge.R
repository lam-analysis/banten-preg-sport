## LOAD PACkAGES

pkgs <- c("magrittr")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)


## PREP

# Read all raw data
tbls <- list(
  "screening" = readxl::read_excel("data/raw/Skrining_Awal.xlsx"),
  "pre"       = readxl::read_excel("data/raw/Pra_Uji_Jalan.xlsx"),
  "post"      = readxl::read_excel("data/raw/Pos_Uji_Jalan.xlsx"),
  "workout"   = readxl::read_excel(
    "data/raw/Skrining_Awal.xlsx", sheet = "woRepeat"
  )
)

# Drop columns consisting only NA
tbls %<>% lapply(function(tbl) {
  tbl %>% subset(select = which({is.na(.) %>% colSums()} < nrow(.)))
})

# Set numeric variables
tbls$screening$age %<>% as.numeric()
tbls$pre$map       %<>% as.numeric()
tbls$post$map      %<>% as.numeric()

# Set date variables
tbls$screening$birthDate %<>% as.Date()


## MERGE

# Merge all data frames but workout history
tbl <- with(tbls, merge(
  subset(screening, select = c(birthDate, age:histOther)),
  merge(
    subset(pre,  select = c(registrar_001, SpO2:map)),
    subset(post, select = c(registrar_001, SpO2:distance)),
    suffixes = c("_pre", "_post"),
    by = "registrar_001"
  ),
  by.x = "registrar",
  by.y = "registrar_001"
))


## WRITE

readr::write_csv(tbl, "data/processed/data.csv")
